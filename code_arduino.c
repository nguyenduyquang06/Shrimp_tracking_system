 //For more Information visit www.aeq-web.com?ref=arduinoide

#include <SoftwareSerial.h>
#include <Wire.h>
//Include thư viện
#include <OneWire.h>
#include <DallasTemperature.h>
#include "DHT.h"
 
// Chân nối với Arduino
#define ONE_WIRE_BUS 9
#define DHTPIN 8
#define LED_GPRS 10
#define LED_PUMP 11
#define LED_CANHBAO 12
//Cài đặt loại DHT11
#define DHTTYPE DHT11

//Thiết đặt thư viện onewire
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);         
SoftwareSerial mySerial(4, 3);            // RX, TX Pins
String apn = "m3-world";                       //APN
String apn_u = "mms";                     //APN-Username
String apn_p = "mms";                     //APN-Password
String data1;   //String for the first Paramter (e.g. Sensor1)
String content = "";
bool pump=false;
bool gprs=true;
bool canhbao=false;
bool setnguong=false;
bool sms=false;
int count=0;
float humid,temp;

int lower_temp_ds=-1,upper_temp_ds=-1;
DHT dht(DHTPIN, DHTTYPE);

//CAM BIEN LUU LUONG NUOC

byte sensorInterrupt = 0;  // 0 = digital pin 2
byte sensorPin       = 2;
// The hall-effect flow sensor outputs approximately 4.5 pulses per second per
// litre/minute of flow.
volatile byte pulseCount;  
float flowRate;
unsigned long oldTime;


void setup() {
  //Thiết lập chân
  pinMode(LED_GPRS, OUTPUT);
  pinMode(LED_CANHBAO, OUTPUT);
  pinMode(LED_PUMP, OUTPUT);
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  pinMode(sensorPin, INPUT);
  digitalWrite(sensorPin, HIGH);
  pulseCount        = 0;
  flowRate          = 0.0;
  oldTime           = 0;
  attachInterrupt(sensorInterrupt, pulseCounter, FALLING);
  dht.begin();
  sensors.begin();
  // set the data rate for the SoftwareSerial port
  mySerial.begin(9600);
  gsm_inithttp();
  delay(10000);
}

void loop() { // run over and over
  if((millis() - oldTime) > 1000) {
    detachInterrupt(sensorInterrupt);
    flowRate = ((1000.0 / (millis() - oldTime)) * pulseCount) / 4.5;
    oldTime = millis();
    
    // Print the flow rate for this second in litres / minute
    Serial.print("LUU LUONG NUOC: ");
    Serial.print(flowRate);  // Print the integer part of the variable
    Serial.print("L/min");    
    // Reset the pulse counter so we can start incrementing again
    pulseCount = 0;
    
    // Enable the interrupt again now that we've finished sending output
    attachInterrupt(sensorInterrupt, pulseCounter, FALLING);
  }
    sensors.requestTemperatures(); 
    data1=sensors.getTempCByIndex(0);    
    Serial.print("Nhiet do tu cam bien:");
    Serial.println(data1); // vì 1 ic nên dùng 0        
    temp = dht.readTemperature();
    Serial.print("Nhiet do tu DHT11:");
    Serial.println(temp); // vì 1 ic nên dùng 0   
    humid = dht.readHumidity();
    Serial.print("Do am:");
    Serial.println(humid); // vì 1 ic nên dùng 0   
    if (gprs) {
      // TODO : Chế độ Online tracking
      gsm_sendhttp(); //Start the GSM-Modul and start the transmisson
      delay(20000); //Wait one half minute
      gsm_receivehttp();
      get_state();
      if (canhbao) {
        send_sms("HE THONG MAT ON DINH, KIEM TRA NGAY");
      }           
      if (sms) {        
        send_sms(get_state());      
        delay(15000); //Wait one half minute
      }
    }
    else {
      // Chế độ Offline tracking, gửi sms      
      send_sms(get_state());      
      delay(60000); //Wait one half minute      
    }    
    delay(10000);    
}

void gsm_inithttp() {
  mySerial.println("AT");
  runsl();//Print GSM Status an the Serial Output;
  delay(4000);
  mySerial.println("AT+SAPBR=3,1,Contype,GPRS");
  runsl();
  delay(1000);
  mySerial.println("AT+SAPBR=3,1,APN," + apn);
  runsl();
  delay(1000);
  mySerial.println("AT+SAPBR=3,1,USER," + apn_u); //Comment out, if you need username
  runsl();
  delay(1000);
  mySerial.println("AT+SAPBR=3,1,PWD," + apn_p); //Comment out, if you need password
  runsl();
  delay(1000);
  mySerial.println("AT+SAPBR =1,1");
  runsl();
  delay(2000);
  mySerial.println("AT+SAPBR=2,1");
  runsl();
  delay(1000);
  mySerial.println("AT+HTTPINIT");
  runsl();
  delay(10000);
  mySerial.println("AT+HTTPPARA=CID,1");
  runsl();
  delay(10000);
}

void gsm_sendhttp() {  
  mySerial.println("AT+HTTPPARA=URL,http://testgprs007.000webhostapp.com/post.php");
  //runsl();
  delay(1000);
  mySerial.println("AT+HTTPPARA=CONTENT,application/x-www-form-urlencoded");
  //runsl();
  delay(1000);
  mySerial.println("AT+HTTPDATA=192,10000");
  //runsl();
  delay(1000);
  mySerial.println("temp_ds=" + data1 + "&temp_dht=" + temp + "&humid_dht=" + humid + "&flowrate=" + flowRate); 
  //runsl();
  delay(10000);
  mySerial.println("AT+HTTPACTION=1");
  //runsl();
  delay(5000);
  mySerial.println("AT+HTTPREAD");
  //runsl();
  delay(1000);
}

void gsm_receivehttp() {  
  mySerial.println("AT+HTTPPARA=URL,http://testgprs007.000webhostapp.com/get.php");
  //runsl();
  delay(5000);
  mySerial.println("AT+HTTPACTION=0");
  //runsl();
  
  delay(20000);  
  mySerial.println("AT+HTTPREAD");  
  //runsl();  
  controller();  
  delay(3000);
}


//Print GSM Status
void runsl() { 
  while (mySerial.available()) {
    Serial.write(mySerial.read());
  }
}

void controller()
{
 content="";
// String RedState = content.substring();
 while (mySerial.available())
 {      
    //Serial.write(mySerial.read());
    content = content + String(char (mySerial.read()));    
 }
 if (content.indexOf("p=1")!=-1) {
      pump=true; 
      // code điều khiển mở bơm
 }
 else if (content.indexOf("p=2")!=-1)
 {
   pump=false;
   //code điều khiển tắt bơm
 }
 if (content.indexOf("g=2")!=-1) {
   if (!gprs) {
     gsm_inithttp();
   }
   gprs=true; 
   sms=false;
 }
 else if (content.indexOf("g=1")!=-1)
 {
   if (gprs) {
     mySerial.println("AT+HTTPTERM");
     delay(500);
     mySerial.println("AT+SAPBR=0,1");
     delay(500);
   }
   gprs=false;
   sms=false;
 }
 else if (content.indexOf("g=3")!=-1) {
  sms=true;
  gprs=true;
 }
 
  
// int lower_temp_dht = content.substring(content.indexOf("=",content.indexOf("temp_dht"))+1,content.indexOf("~",content.indexOf("temp_dht"))).toInt();
// int upper_temp_dht = content.substring(content.indexOf("~",content.indexOf("temp_dht"))+1,content.indexOf(";humid_dht")).toInt();  
//
// int lower_humid_dht = content.substring(content.indexOf("=",content.indexOf("humid_dht"))+1,content.indexOf("~",content.indexOf("humid_dht"))).toInt();
// int upper_humid_dht = content.substring(content.indexOf("~",content.indexOf("humid_dht"))+1,content.length()).toInt();  
 if (count<2) {
  count++;
 }
 else if (count>1) {
  lower_temp_ds = content.substring(content.indexOf("=",content.indexOf("tn"))+1,content.indexOf("~",content.indexOf("tn"))).toInt();
  upper_temp_ds = content.substring(content.indexOf("~",content.indexOf("tn"))+1,content.indexOf(";tt")).toInt();  
  Serial.println(lower_temp_ds);
  Serial.println(upper_temp_ds);
  if (lower_temp_ds==-1 && lower_temp_ds==-1) {
    setnguong=false;
  }
  else {
    setnguong=true;
  }
 } 
 content="";
}


String get_state() 
{
  String result="";
 if (pump) {
   Serial.println("BOM DANG MO");
   digitalWrite(LED_PUMP, HIGH);
   result+="BOM DANG MO,";
 }
 else {
   Serial.println("BOM DANG TAT");
   digitalWrite(LED_PUMP, LOW);
   result+="BOM DANG TAT,";
 }
 if (gprs) {
   Serial.println("GPRS DANG MO");
   digitalWrite(LED_GPRS, HIGH);
   result+="GPRS DANG MO,";
 }
 else {
   Serial.println("GPRS DANG TAT");
   digitalWrite(LED_GPRS, LOW);
   result+="GPRS DANG TAT,";
 }
  if (setnguong) {
    if (data1.toInt()<lower_temp_ds) {
      //code canh bao be hon can duoi
      Serial.println("Vuot muc canh bao, he thong tu bom nuoc nong");        
      canhbao=true; 
    }
    else if (data1.toInt()>upper_temp_ds) {
      //code canh bao be hon can tren
      Serial.println("Vuot muc canh bao, he thong tu bom nuoc lanh");    
      canhbao=true;      
    }
    else {
      canhbao=false;
    }
 }
 else {
  canhbao=false;
 }
  if (canhbao) {
   Serial.println("CAM BIEN DANG VUOT NGUONG");
   digitalWrite(LED_CANHBAO, HIGH);
   result+="CAM BIEN VUOT NGUONG";   
 }
 else {
   Serial.println("HE THONG ON DINH");
   digitalWrite(LED_CANHBAO, LOW);     
   result+="HE THONG ON DINH";
 } 
 result += "t duoinuoc " + (String) data1 + " Do am " + (String) humid + " Luu luong " + (String)flowRate;
 return result;
}
void send_sms(String text) {
   mySerial.print("\r");
   delay(1000);                  
   mySerial.print("AT+CMGF=1\r");    
   delay(1000);
   /*Replace XXXXXXXXXX to 10 digit mobile number &  ZZ to 2 digit country code*/
   mySerial.print("AT+CMGS=\"+84932069973\"\r");    
   delay(1000);
   //The text of the message to be sent.
   mySerial.print(text);   
   delay(1000);
   mySerial.write(0x1A);
   delay(1000); 
}

void pulseCounter()
{
  // Increment the pulse counter
  pulseCount++;
}
