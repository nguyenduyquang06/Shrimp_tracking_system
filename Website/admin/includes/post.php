<?php
$temp_ds = $_POST["temp_ds"];
$temp_dht = $_POST["temp_dht"];
$humid_dht = $_POST["humid_dht"];
$user = $_POST["username"];
$passwd = $_POST["password"];
if (empty($temp_ds) || empty($user) || empty($passwd) ) {
    echo 'Cung cap day du thong tin POST';
}
else {
    require('sensordb.php');
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    } 
    date_default_timezone_set('Asia/Ho_Chi_Minh');
    $timestamp = date("Y-m-d H:i:s");
    $sql = 
    "SET @m = (SELECT MAX(id) + 1 FROM templog); 
    SET @s = CONCAT('ALTER TABLE templog AUTO_INCREMENT=', @m);
    PREPARE stmt1 FROM @s;
    EXECUTE stmt1;
    DEALLOCATE PREPARE stmt1;     
    INSERT INTO templog (temp_ds,temp_dht,humid_dht,date_log) VALUES ('$temp_ds','$temp_dht','$humid_dht','$timestamp');";
    if ($conn->multi_query($sql) === TRUE) {
        echo "New record created successfully\n";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
    $conn->close();
    $txt = $timestamp . " : ".$temp_ds." : ".$temp_dht." : ".$humid_dht."\n";
    echo $txt;
    /* 
    $myfile = fopen("newfile.txt", "w") or die("Unable to open file!");
    fwrite($myfile, $txt);
    fclose($myfile);
*/
}
?>