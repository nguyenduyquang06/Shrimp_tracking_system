<?php
session_start();
error_reporting(0);
include('includes/config.php');
if(strlen($_SESSION['login'])==0)
  { 
header('location:index.php');
}
else{?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Hệ thống nuôi tôm thông minh | Bảng giám sát</title>
    <!-- BOOTSTRAP CORE STYLE  -->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONT AWESOME STYLE  -->
    <link href="assets/css/te.css" rel="stylesheet" />
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLE  -->
    <link href="assets/css/style.css" rel="stylesheet" />
    <!-- GOOGLE FONT -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <link rel="stylesheet"  href="assets/css/elements.css" />
    <script src="assets/js/my_js.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
    <script type="text/javascript" src="http://static.fusioncharts.com/code/latest/fusioncharts.js"></script>
    <script type="text/javascript" src="assets/js/te.js"></script>
    <script type="text/javascript" src="http://static.fusioncharts.com/code/latest/themes/fusioncharts.theme.fint.js?cacheBust=56"></script>
    <style>#temp-container {
            border-style: none;
            float: left;
            box-sizing: border-box;
        }
    </style>
</head>
<body>
<?php include('includes/header.php');?>
<!-- MENU SECTION END-->
    <div class="content-wrapper">
         <div class="container">
        <div class="row pad-botm">
            <div class="col-md-12">
                <h4 class="header-line">GIÁM SÁT ĐẦM TÔM</h4>
<!--                <button id="gprs" type="button" class="btn btn-primary pull-right">Tắt GPRS</button><button id="pump" type="button" class="btn btn-primary pull-left">Mở máy bơm</button>-->
            </div>
        </div>
             <div class="row">
                 <div class="col-md-3 col-sm-3 col-xs-6">
                      <div class="alert alert-info back-widget-set text-center">
                          <img src="images/tom_thuong.png" alt="Tôm" style="width:70px;height:70px;">
                                <?php
                                require('librarydb.php');
                                // Create connection
                                // Check connection
                                if ($conn->connect_error) {
                                    die("Connection failed: " . $conn->connect_error);
                                }
                                $sql =
                                    "SELECT breeder,shrimp FROM state_farm WHERE id=(SELECT max(id) FROM state_farm);";
                                $result = mysqli_query($conn,$sql) or die(mysqli_error());
                                $row = mysqli_fetch_assoc($result);
                                //echo $row['value'];
                                $conn->close();
                                ?>
                          <script type="text/javascript">var tempVal = <?php echo json_encode($row['shrimp']); ?>;</script>
                          <!--<div style="margin:0 auto;line-height: 50px;" class="" id="temp-container"></div>-->
                          <h3><?php echo $row['shrimp'];?> con</h3>
                          Tôm đang nuôi

                          <p><button id="shrimp" type="button" class="btn btn-primary">Cập nhật</button></p>
                          <script type="text/javascript">
                              document.getElementById('shrimp').onclick = function(){
                                  var shrimp = prompt('Nhập thông tin');
                                  if (shrimp) {
                                      $.ajax({
                                          type:"POST",
                                          url: "dashboard.php",
                                          data: "shrimp=" + shrimp,
                                          success: function(){
                                              alert("Đã cập nhật " + shrimp + " con vào hệ thống");
                                          }
                                      });
                                      location.reload();
                                  }
                                  else {
                                      alert("Cập nhật lỗi")
                                  }
                              }
                          </script>
                          <?php
                          require('librarydb.php');
                          // Create connection
                          // Check connection
                          $shrimp = $_POST['shrimp'];
                          if (!empty($shrimp)) {
                              if ($conn->connect_error) {
                                  die("Connection failed: " . $conn->connect_error);
                              }
                              $sql =
                                  "UPDATE state_farm SET shrimp = '$shrimp' WHERE id = 1;";
                              $result = mysqli_query($conn,$sql) or die(mysqli_error());
                              $conn->close();
                          }
                          ?>
                        </div>

                    </div>
             
               <div class="col-md-3 col-sm-3 col-xs-6">
                      <div class="alert alert-warning back-widget-set text-center">
                          <img src="images/tom_giong.png" alt="Tôm" style="width:70px;height:70px;">
                          <script type="text/javascript">var tempVal = <?php echo json_encode($row['breeder']); ?>;</script>
                          <!--<div style="margin:0 auto;line-height: 50px;" class="" id="temp-container"></div>-->
                          <h3><?php echo $row['breeder'];?> con</h3>
                          Tôm giống
                          <p><button id="breeder" type="button" class="btn btn-primary">Cập nhật</button></p>
                          <script type="text/javascript">
                              document.getElementById('breeder').onclick = function(){
                                  var breeder = prompt('Nhập thông tin');
                                  if (breeder) {
                                      $.ajax({
                                          type:"POST",
                                          url: "dashboard.php",
                                          data: "breeder=" + breeder,
                                          success: function(){
                                              alert("Đã cập nhật " + breeder + " con vào hệ thống");
                                          }
                                      });
                                      location.reload();
                                  }
                                  else {
                                      alert("Cập nhật lỗi")
                                  }
                              }
                          </script>
                          <?php
                          $breeder = $_POST['breeder'];
                          if (!empty($breeder)) {
                              if ($conn->connect_error) {
                                  die("Connection failed: " . $conn->connect_error);
                              }
                              $sql =
                                  "UPDATE state_farm SET breeder = '$breeder' WHERE id = 1;";
                              $result = mysqli_query($conn,$sql) or die(mysqli_error());
                              $conn->close();
                          }
                          ?>
                        </div>
                    </div>
                 <div class="col-md-3 col-sm-3 col-xs-6">
                     <div class="alert alert-success back-widget-set text-center">
                         <i class="fas fa-thermometer-half fa-5x"></i>

                           <?php
                                require('sensordb.php');
                                // Create connection
                                // Check connection
                                if ($conn->connect_error) {
                                    die("Connection failed: " . $conn->connect_error);
                                }
                                $sql =
                                "SELECT temp_ds,temp_dht,humid_dht,flowrate FROM templog WHERE id=(SELECT max(id) FROM templog);";
                                $result = mysqli_query($conn,$sql) or die(mysqli_error());
                                $row = mysqli_fetch_assoc($result);
                                //echo $row['value'];
                                $conn->close();
                            ?>
                         <script type="text/javascript">var tempVal = <?php echo json_encode($row['temp_ds']); ?>;</script>
                         <!--<div style="margin:0 auto;line-height: 50px;" class="" id="temp-container"></div>-->
                         <h3><?php echo $row['temp_ds'];?> ℃</h3>
                         Nhiệt độ dưới nước
                         <p><button id="warning_tempds" type="button" class="btn btn-warning">Set cảnh báo</button> <button id="stop_warning_tempds" type="button" class="btn btn-danger">Tắt cảnh báo</button></p>
                         Ngưỡng cảnh báo:
                         <h3>
                             <?php
                             require('sensordb.php');
                             if ($conn->connect_error) {
                                 die("Connection failed: " . $conn->connect_error);
                             }
                             $sql ="SELECT on_off,pump_controller,lower_temp_ds,upper_temp_ds,lower_temp_dht,upper_temp_dht,lower_humid_dht,upper_humid_dht FROM state_gprs WHERE id=1;";
                             $result = mysqli_query($conn,$sql) or die(mysqli_error());
                             $row1 = mysqli_fetch_assoc($result);
                             if ($row1['lower_temp_ds']==-1 && $row1['upper_temp_ds']==-1)
                                {
                                    echo "Đã tắt cảnh báo";
                                }
                             else{
                                    echo $row1['lower_temp_ds'];  echo '-';   echo $row1['upper_temp_ds'];
                                 }
                             $conn->close();
                             ?>
                         </h3>
                         <script type="text/javascript">
                             document.getElementById('warning_tempds').onclick = function(){
                                 var lower_tempds = prompt('Nhập giới hạn dưới');
                                 var upper_tempds=  prompt('Nhập giới hạn trên');
                                 if (lower_tempds>upper_tempds) {
                                     alert("Cận dưới phải bé hơn, nhập lại");
                                 }
                                 $.ajax({
                                     type:"POST",
                                     url: "dashboard.php",
                                     data: "lower_tempds=" + lower_tempds + "\u0026upper_tempds="+ upper_tempds,
                                     success: function(){
                                     }
                                 });
                                 location.reload();
                             }
                             document.getElementById('stop_warning_tempds').onclick = function(){
                                 var lower_tempds="-1";
                                 var upper_tempds="-1";
                                 $.ajax({
                                     type:"POST",
                                     url: "dashboard.php",
                                     data: "lower_tempds=" + lower_tempds + "\u0026upper_tempds="+ upper_tempds,
                                     success: function(){
                                     }
                                 });
                                 location.reload();
                             }
                         </script>
                         <?php
                         require('sensordb.php');
                         // Create connection
                         // Check connection
                         $lower_tempds = $_POST['lower_tempds'];
                         $upper_tempds = $_POST['upper_tempds'];
                         echo '</script>';
                         if (!empty($lower_tempds) && !empty($upper_tempds)) {
                             if ($conn->connect_error) {
                                 die("Connection failed: " . $conn->connect_error);

                             }
                             $sql =
                                 "UPDATE state_gprs SET lower_temp_ds = '$lower_tempds',upper_temp_ds='$upper_tempds' WHERE id = 1;";
                             $result = mysqli_query($conn,$sql) or die(mysqli_error());
                             $conn->close();
                         }
                         ?>
                     </div>
                 </div>
                 <div class="col-md-3 col-sm-3 col-xs-6">
                     <div class="alert alert-danger back-widget-set text-center">
                         <i class="fas fa-thermometer-half fa-3x"></i>
                         <script type="text/javascript">var tempVal = <?php echo json_encode($row['temp_dht']); ?>;</script>
                         <!--<div style="margin:0 auto;line-height: 50px;" class="" id="temp-container"></div>-->
                         <h3><?php echo $row['temp_dht'];?> ℃</h3>
                         Nhiệt độ môi trường
<!--                         <p><button id="warning_tempdht" type="button" class="btn btn-warning">Set cảnh báo</button> <button id="stop_warning_tempdht" type="button" class="btn btn-danger">Tắt cảnh báo</button></p>-->
<!--                         Ngưỡng cảnh báo:-->
<!--                         <h3>-->
<!--                             --><?php
//
//                             if ($row1['lower_temp_dht']==-1 && $row1['upper_temp_dht']==-1)
//                             {
//                                 echo "Đã tắt cảnh báo";
//                             }
//                             else{
//                                 echo $row1['lower_temp_dht'];  echo '-';   echo $row1['upper_temp_dht'];
//                             }
//                             ?>
<!--                         </h3>-->
                         <script type="text/javascript">
                             document.getElementById('warning_tempdht').onclick = function(){
                                 var lower_tempdht = prompt('Nhập giới hạn dưới');
                                 var upper_tempdht = prompt('Nhập giới hạn trên');
                                 if (lower_tempdht>upper_tempdht) {
                                     alert("Cận dưới phải bé hơn, nhập lại");
                                 }
                                 $.ajax({
                                     type:"POST",
                                     url: "dashboard.php",
                                     data: "lower_tempdht=" + lower_tempdht + "\u0026upper_tempdht="+ upper_tempdht,
                                     success: function(){
                                     }
                                 });
                                 location.reload();
                             }
                             document.getElementById('stop_warning_tempdht').onclick = function(){
                                 var lower_tempdht="-1";
                                 var upper_tempdht="-1";
                                 $.ajax({
                                     type:"POST",
                                     url: "dashboard.php",
                                     data: "lower_tempdht=" + lower_tempdht + "\u0026upper_tempdht="+ upper_tempdht,
                                     success: function(){
                                     }
                                 });
                                 location.reload();
                             }
                         </script>
                         <?php
                         $lower_tempdht = $_POST['lower_tempdht'];
                         $upper_tempdht = $_POST['upper_tempdht'];
                         echo '</script>';
                         if (!empty($lower_tempdht) && !empty($upper_tempdht)) {
                             if ($conn->connect_error) {
                                 die("Connection failed: " . $conn->connect_error);
                             }
                             $sql =
                                 "UPDATE state_gprs SET lower_temp_dht = '$lower_tempdht',upper_temp_dht='$upper_tempdht' WHERE id = 1;";
                             $result = mysqli_query($conn,$sql) or die(mysqli_error());
                             $conn->close();
                         }
                         ?>
                     </div>
                 </div>
                 <div class="col-md-3 col-sm-3 col-xs-6">
                     <div class="alert alert-danger back-widget-set text-center">
                         <i class="fas fa-thermometer-half fa-3x"></i>
                         <script type="text/javascript">var tempVal = <?php echo json_encode($row['humid_dht']); ?>;</script>
                         <!--<div style="margin:0 auto;line-height: 50px;" class="" id="temp-container"></div>-->
                         <h3><?php echo $row['humid_dht'];?>%</h3>
                         Độ ẩm môi trường
<!--                         <p><button id="warning_humiddht" type="button" class="btn btn-warning">Set cảnh báo</button> <button id="stop_warning_humiddht" type="button" class="btn btn-danger">Tắt cảnh báo</button></p>-->
<!--                         Ngưỡng cảnh báo:-->
<!--                         <h3>-->
<!--                             --><?php
//
//                             if ($row1['lower_humid_dht']==-1 && $row1['upper_humid_dht']==-1)
//                             {
//                                 echo "Đã tắt cảnh báo";
//                             }
//                             else{
//                                 echo $row1['lower_humid_dht'];  echo '-';   echo $row1['upper_humid_dht'];
//                             }
//                             ?>
<!--                         </h3>-->
                     </div>
                     <script type="text/javascript">
                         document.getElementById('warning_humiddht').onclick = function(){
                             var lower_humiddht = prompt('Nhập giới hạn dưới');
                             var upper_humiddht = prompt('Nhập giới hạn trên');
                             if (lower_humiddht>upper_humiddht) {
                                 alert("Cận dưới phải bé hơn, nhập lại");
                             }
                             $.ajax({
                                 type:"POST",
                                 url: "dashboard.php",
                                 data: "lower_humiddht=" + lower_humiddht + "\u0026upper_humiddht="+ upper_humiddht,
                                 success: function(){
                                 }
                             });
                             location.reload();
                         }
                         document.getElementById('stop_warning_humiddht').onclick = function(){
                             var lower_humiddht = -1;
                             var upper_humiddht = -1;
                             $.ajax({
                                 type:"POST",
                                 url: "dashboard.php",
                                 data: "lower_humiddht=" + lower_humiddht + "\u0026upper_humiddht="+ upper_humiddht,
                                 success: function(){
                                 }
                             });
                             location.reload();
                         }
                     </script>
                     <?php
                     $lower_humiddht = $_POST['lower_humiddht'];
                     $upper_humiddht = $_POST['upper_humiddht'];
                     echo '</script>';
                     if (!empty($lower_humiddht) && !empty($upper_humiddht)) {
                         if ($conn->connect_error) {
                             die("Connection failed: " . $conn->connect_error);
                         }
                         $sql =
                             "UPDATE state_gprs SET lower_humid_dht = '$lower_humiddht',upper_humid_dht='$upper_humiddht' WHERE id = 1;";
                         $result = mysqli_query($conn,$sql) or die(mysqli_error());
                         $conn->close();
                     }
                     ?>
                 </div>
                 <div class="col-md-3 col-sm-3 col-xs-6">
                     <div class="alert alert-danger back-widget-set text-center">
                         <i class="fas fa-thermometer-half fa-5x"></i>
                         <script type="text/javascript">var tempVal = <?php echo json_encode($row['temp_dht']); ?>;</script>
                         <!--<div style="margin:0 auto;line-height: 50px;" class="" id="temp-container"></div>-->
                         <h3><?php echo $row['flowrate'];?> Lít / phút</h3>
                         Lưu lượng dòng chảy
                     </div>
                 </div>
                 <div class="col-md-3 col-sm-3 col-xs-6">
                     <div class="alert alert-info back-widget-set text-center">
                         <img src="images/pump.png" alt="Máy Bơm" style="width:100px;height:70px;">
                         <script type="text/javascript">var tempVal = <?php echo json_encode($row['breeder']); ?>;</script>
                         <!--<div style="margin:0 auto;line-height: 50px;" class="" id="temp-container"></div>-->
                         <h3> BƠM </h3>

                         <h3 id="state_pump" style="color: red" > Đang tắt </h3>
                         <p><button id="pump" type="button" class="btn btn-primary">Bơm</button></p>
                         <script type="text/javascript">
                             document.getElementById('pump').onclick = function(){
                                 var pump;
                                 if (document.getElementById('pump').innerHTML=="Mở máy bơm") {
                                     alert("máy bơm đang mở");
                                     pump = 1;
                                 }
                                 else if ((document.getElementById('pump').innerHTML=="Tắt máy")) {
                                     alert("máy bơm đang tắt");
                                     pump = 2;
                                 }
                                 $.ajax({
                                     type:"POST",
                                     url: "dashboard.php",
                                     data: "pump=" + pump,
                                     success: function(){
                                     }
                                 });
                                 location.reload();
                             }
                         </script>
                         <?php
                         require('sensordb.php');
                         // Create connection
                         // Check connection
                         if ($conn->connect_error) {
                             die("Connection failed: " . $conn->connect_error);
                         }
                         $sql =
                             "SELECT pump_controller,on_off FROM state_gprs WHERE id=1;";
                         $result = mysqli_query($conn,$sql) or die(mysqli_error());
                         $rowp = mysqli_fetch_assoc($result);
                         if ($rowp['pump_controller']==2) {
                             echo '<script language="javascript">';
                             echo 'document.getElementById(\'state_pump\').innerHTML="Đang tắt";';
                             //echo 'document.getElementById(\'state_pump\').style.color="red";';
                             echo 'document.getElementById(\'pump\').innerHTML="Mở máy bơm";';
                             echo '</script>';
                         }
                         else if ($rowp['pump_controller']==1) {
                             echo '<script language="javascript">';
                             echo 'document.getElementById(\'state_pump\').innerHTML="Đang mở";';
                             echo 'document.getElementById(\'pump\').innerHTML="Tắt máy";';
                             echo 'document.getElementById(\'state_pump\').style.color="green";';
                             echo '</script>';
                         }
                         $pump=$_POST['pump'];
                         if (!empty($pump)) {
                             $sql =
                                 "UPDATE state_gprs SET pump_controller = '$pump' WHERE id = 1;";
                             $result = mysqli_query($conn,$sql) or die(mysqli_error());
                         }
                         $conn->close();
                         ?>

                     </div>
                 </div>
                 <div class="col-md-3 col-sm-3 col-xs-6">
                 <div class="alert alert-danger back-widget-set text-center">
                     <img src="images/gprs.jpg" alt="Máy Bơm" style="width:70px;height:70px;">
                     <script type="text/javascript">var tempVal = <?php echo json_encode($row['breeder']); ?>;</script>
                     <!--<div style="margin:0 auto;line-height: 50px;" class="" id="temp-container"></div>-->
                     <h3> GPRS </h3>
                     <h3 id="state_gprs" style="color: red" > Đang tắt </h3>
                     <p><button id="gprs" type="button" class="btn btn-primary">Bơm</button></p>
                     <p><button style="display: none;" id="sms" type="button" class="btn btn-primary">Bật gửi tin nhắn</button></p>
                     <script type="text/javascript">
                         document.getElementById('gprs').onclick = function(){
                             var gprs;
                             if (document.getElementById('gprs').innerHTML=="Tắt GPRS") {
                                 alert("Đang tắt GPRS");
                                 gprs = 1;
                             }
                             else if ((document.getElementById('gprs').innerHTML=="Mở GPRS")) {
                                 alert("Đang mở GPRS");
                                 gprs = 2;
                             }
                             $.ajax({
                                 type:"POST",
                                 url: "dashboard.php",
                                 data: "gprs=" + gprs,
                                 success: function(){
                                 }
                             });
                             location.reload();
                         }
                         document.getElementById('sms').onclick = function(){
                             var gprs;
                             if (document.getElementById('state_gprs').innerHTML=="Đang mở") {
                                 alert("Mở gửi tin nhắn");
                                 gprs = 3;
                             }
                             else if ((document.getElementById('state_gprs').innerHTML=="Đang mở + SMS")) {
                                 alert("Tắt gửi tin nhắn");
                                 gprs = 2;
                             }
                             $.ajax({
                                 type:"POST",
                                 url: "dashboard.php",
                                 data: "gprs=" + gprs,
                                 success: function(){
                                 }
                             });
                             location.reload();
                         }
                     </script>
                     <?php
                     require('sensordb.php');
                     // Create connection
                     // Check connection
                     if ($conn->connect_error) {
                         die("Connection failed: " . $conn->connect_error);
                     }
                     $sql =
                         "SELECT pump_controller,on_off FROM state_gprs WHERE id=1;";
                     $result = mysqli_query($conn,$sql) or die(mysqli_error());
                     $rowp = mysqli_fetch_assoc($result);
                     if ($rowp['on_off']==1) {
                         echo '<script language="javascript">';
                         echo 'document.getElementById(\'state_gprs\').innerHTML="Đang tắt";';
                         //echo 'document.getElementById(\'state_pump\').style.color="red";';
                         echo 'document.getElementById(\'gprs\').innerHTML="Mở GPRS";';
                         echo '</script>';
                     }
                     else if ($rowp['on_off']==2) {
                         echo '<script language="javascript">';
                         echo 'document.getElementById(\'state_gprs\').innerHTML="Đang mở";';
                         echo 'document.getElementById(\'gprs\').innerHTML="Tắt GPRS";';
                         echo 'document.getElementById(\'state_gprs\').style.color="green";';
                         echo 'document.getElementById(\'sms\').style.display="inline";';
                         echo '</script>';
                     }
                     else if ($rowp['on_off']==3) {
                         echo '<script language="javascript">';
                         echo 'document.getElementById(\'state_gprs\').innerHTML="Đang mở + SMS";';
                         echo 'document.getElementById(\'gprs\').innerHTML="Tắt GPRS";';
                         echo 'document.getElementById(\'state_gprs\').style.color="green";';
                         echo 'document.getElementById(\'sms\').innerHTML="Tắt gửi tin nhắn";';
                         echo 'document.getElementById(\'sms\').style.display="inline";';
                         echo '</script>';
                     }
                     $gprs=$_POST['gprs'];
                     if (!empty($gprs)) {
                         $sql =
                             "UPDATE state_gprs SET on_off = '$gprs' WHERE id = 1;";
                         $result = mysqli_query($conn,$sql) or die(mysqli_error());
                     }
                     $conn->close();
                     ?>
                 </div>
             </div>
        </div>


            
    </div>
    </div>
     <!-- CONTENT-WRAPPER SECTION END-->
<?php include('includes/footer.php');?>
      <!-- FOOTER SECTION END-->
    <!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <!-- CORE JQUERY  -->
    <script src="assets/js/jquery-1.10.2.js"></script>
    <!-- BOOTSTRAP SCRIPTS  -->
    <script src="assets/js/bootstrap.js"></script>
      <!-- CUSTOM SCRIPTS  -->
    <script src="assets/js/custom.js"></script>
</body>
</html>
<?php } ?>
