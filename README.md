# Nhiệm vụ đề tài

- Xây dựng một mô hình giám sát với các cảm biến nhiệt độ, độ ẩm, lưu lượng nước, hiển thị những thông tin cần thiết trên Web hoặc dưới dạng tin nhắn cho người giám sát. 
- Ngôn ngữ và framework: 
 Sử dụng *PHP* cho lập trình backend web. *HTML CSS Javascript* Front-end, *MySQL* cho database


# Algorithm flowchart

<p>
<img src="https://i1274.photobucket.com/albums/y433/duyquang6/Annotation%202018-12-08%20091245_zpszmfsl5z6.jpg" />
<img src="https://i1274.photobucket.com/albums/y433/duyquang6/Annotation%202018-12-08%20091306_zpsjy1vqqs2.jpg" />
</p>

# Presentation
<p>
<img src="https://i1274.photobucket.com/albums/y433/duyquang6/Untitled_zpsxrfxfib5.png" />
</p>